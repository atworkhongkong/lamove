<?php

namespace App\Classes;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Order
{
    public function listOrders($data)
    {
        $rpp = $data['limit'] ?? Config::get('site-config.rpp');
        $results = DB::table('orders')->select('id', 'distance', 'status')->orderBy('created_at')->paginate($rpp);
        $results->appends(['limit' => $data['limit']]);
        return $results;
    }

    public function createOrder($order)
    {
        try {
            $orderId = DB::table('orders')->insertGetId($order);
            return $orderId;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function updateOrder($id, $status)
    {
        DB::beginTransaction();
        DB::table('orders')->where('id', $id)->lockForUpdate()->get();
        DB::table('orders')->where('id', $id)->update(['status' => $status]);
        DB::commit();
    }
}
