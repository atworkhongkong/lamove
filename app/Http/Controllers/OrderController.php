<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Classes\Order;
use App\Classes\Helper;

class OrderController extends Controller
{
    private $orderObj;
    private $helperObj;

    public function __construct(Order $orderObj, Helper $helperObj)
    {
        $this->orderObj = $orderObj;
        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        $rules = [
            'page' => 'required|numeric|min:1',
            'limit' => 'required|numeric|min:1'
        ];
        $messages = [
            'page.required' => 'Please enter page number',
            'page.numeric' => 'Please enter a valid page number',
            'page.min' => 'Please enter a page number equal or larger than 1',
            'limit.required' => 'Please enter page limit',
            'limit.numeric' => 'Please enter a valid limit number',
            'limit.min' => 'Please enter a limit number equal or larger than 1'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json($errors);
        }
        $results = $this->orderObj->listOrders(request()->all());
        return $results;
    }

    public function store(Request $request)
    {
        $originalLat = $request->input('origin.START_LATITUDE') ?? 22.321550;
        $originalLng = $request->input('origin.START_LONGITUDE') ?? 114.171112;
        $destinationLat = $request->input('origin.END_LATITUDE') ?? 22.377131;
        $destinationLng = $request->input('origin.END_LONGITUDE') ?? 114.197441;

        $startValid = $endValid = true;
        if (!$this->helperObj->validateLatitude($originalLat) || !$this->helperObj->validateLongitude($originalLng)) {
            $startValid = false;
        }
        if (!$this->helperObj->validateLatitude($destinationLat) || !$this->helperObj->validateLongitude($destinationLng)) {
            $endValid = false;
        }
        if (!$startValid) {
            return response()->json(['error' => 'Please input a valid start location'], 400);
        }
        if (!$endValid) {
            return response()->json(['error' => 'Please input a valid end location'], 400);
        }
        //$distance = $this->helperObj->distance(32.9697, -96.80322, 29.46786, -98.53506, "K");
        $distance = $this->helperObj->distance($originalLat, $originalLng, $destinationLat, $destinationLng, "K");
        $distance = number_format($distance, 2);
        $order = [
            'start_lat' => $originalLat,
            'start_lng' => $originalLng,
            'end_lat' => $destinationLat,
            'end_lng' => $destinationLng,
            'distance' => $distance,
            'status' => 'UNASSIGNED',
            'created_at' => NOW()
        ];
        $id = $this->orderObj->createOrder($order);
        $return = [
            'id' => $id,
            'distance' => $distance,
            'status' => 'UNASSIGNED'
        ];
        return response()->json($return, 200);
    }

    public function update(Request $request, $id)
    {
        $status = $request->input('status');
        $status = 'TAKEN';
        if (!in_array($status, ['TAKEN'])) {
            return response()->json(['error' => 'Please input a valid status'], 400);
        }
        $id = intval($id);
        $this->orderObj->updateOrder($id, $status);
        return response()->json(['status' => 'SUCCESS'], 200);
    }
}
