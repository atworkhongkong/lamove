<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin API Form</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
</head>
<body>
<div id="app">
    <form method="GET" action="orders">
        <p>List order</p>
        <div>
            <label for="page">Page</label>
            <input id="page" type="number" name="page" value="1" />
        </div>
        <div>
            <label for="limit">Limit</label>
            <input id="limit" type="number" name="limit" value="10" />
        </div>
        <div>
            <button type="submit">Submit</button>
        </div>
    </form>

    <hr>

    <form method="POST" action="orders">
        <input type="hidden" name="_token" value="asdfasdf9safasfas0">
        <p>Create order</p>
        <div>
            <button type="submit">Submit</button>
        </div>
    </form>

    <hr>

    <form method="POST" action="orders/1">
        <input type="hidden" name="_token" value="asdfasdf9safasfas0">
        {{ method_field('PATCH') }}
        <p>Take order #1</p>
        <div>
            <button type="submit">Submit</button>
        </div>
    </form>
</div>
</body>

<script src="/js/app.js"></script>
</html>
