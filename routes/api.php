<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/api-form', function() {
    return view('api-form');
});
Route::get('/orders', 'OrderController@index')->name('order.index');
Route::post('/orders', 'OrderController@store')->name('order.store');
Route::PATCH('/orders/{id}', 'OrderController@update')->name('order.update');
