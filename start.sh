#!/bin/bash

echo "Installing dependencies..."

# install composer packages
composer install


###
# Laravel configuration
###
echo "Installing dependencies..."

php artisan migrate

# start laravel server
php artisan serve
